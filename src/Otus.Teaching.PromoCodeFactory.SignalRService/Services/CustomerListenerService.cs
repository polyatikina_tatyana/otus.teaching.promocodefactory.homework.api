﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.SignalRService.Hubs;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRService.Services
{
    public class CustomerListenerService : BackgroundService
    {
        private readonly IHubContext<CustomerHub> _hubContext;
        private readonly IServiceProvider _seviceProvider;

        public CustomerListenerService(
            IServiceProvider seviceProvider,
            IHubContext<CustomerHub> hubContext)
        {
            _hubContext = hubContext;
            _seviceProvider = seviceProvider;
        }

        private long? _countCustomers;

        private async Task OnCustomerUpdated()
        {
            using var scope = _seviceProvider.CreateScope();
            var customerService = scope.ServiceProvider.GetRequiredService<ICustomerService>();
            var currentCount = await customerService.GetCustomersCount();
            if (_countCustomers.HasValue && _countCustomers != currentCount)
            {
                var group = _hubContext.Clients.Group("all");
                await group.SendAsync("RefreshCustomers", "Refresh list customer");
            }
            _countCustomers = currentCount;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await OnCustomerUpdated();
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}
