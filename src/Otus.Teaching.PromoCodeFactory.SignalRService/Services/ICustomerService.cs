﻿using Otus.Teaching.PromoCodeFactory.SignalRService.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRService.Services
{
    public interface ICustomerService
    {
        Task<CustomerResponse> Create(CreateOrEditCustomerRequest request);
        Task<CustomerResponse> Edit(CreateOrEditCustomerRequest request);
        Task Delete(Guid id);
        Task<long> GetCustomersCount();
        Task<CustomerResponse> GetCustomer(Guid id);
        Task<IEnumerable<CustomerResponse>> GetCustomers(string searchText);
    }
}
