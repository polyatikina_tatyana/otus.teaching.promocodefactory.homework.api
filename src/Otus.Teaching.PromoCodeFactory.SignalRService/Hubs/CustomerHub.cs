﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.SignalRService.Models;
using Otus.Teaching.PromoCodeFactory.SignalRService.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRService.Hubs
{
    public class CustomerHub : Hub
    {
        private readonly IServiceProvider _seviceProvider;

        public CustomerHub(IServiceProvider seviceProvider) { _seviceProvider = seviceProvider; }

        public async Task<string> SayHello(string name)
        {
            var response = $"Hello {name}";

            await Clients.Client(Context.ConnectionId).SendAsync("Notify", response);

            return response;
        }

        public async Task<CustomerResponse> Create(CreateOrEditCustomerRequest customer)
        {
            using var scope = _seviceProvider.CreateScope();
            var customerService = scope.ServiceProvider.GetRequiredService<ICustomerService>();
            var response = await customerService.Create(customer);
            if (response != null)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customer created");
                await Clients.AllExcept(new List<string> { Context.ConnectionId }).SendAsync("CreateCustomer", response);
            }
            else
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customer invalid data");
            }

            return response;
        }

        public async Task<CustomerResponse> Edit(CreateOrEditCustomerRequest customer)
        {
            using var scope = _seviceProvider.CreateScope();
            var customerService = scope.ServiceProvider.GetRequiredService<ICustomerService>();
            var response = await customerService.Edit(customer);
            if (response == null)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customer not fount");

            }

            await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customer edit");
            await Clients.AllExcept(new List<string> { Context.ConnectionId }).SendAsync("EditCustomer", response);

            return response;
        }

        public async Task Delete(string id)
        {
            if (!Guid.TryParse(id, out var customerId))
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Incorrect customer id");
                return;
            }

            using var scope = _seviceProvider.CreateScope();
            var customerService = scope.ServiceProvider.GetRequiredService<ICustomerService>();
            await customerService.Delete(customerId);

            await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customer delete");
            await Clients.AllExcept(new List<string> { Context.ConnectionId }).SendAsync("DeleteCustomer", id);
        }

        public async Task<CustomerResponse> Customer(string id)
        {
            if (!Guid.TryParse(id, out var customerId))
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Incorrect customer id");
                return null;
            }

            using var scope = _seviceProvider.CreateScope();
            var customerService = scope.ServiceProvider.GetRequiredService<ICustomerService>();
            var customer = await customerService.GetCustomer(customerId);
            if (customer == null)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customer not fount");
            
            }
            else
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Customer", customer);
            }

            return customer;
        }

        public async Task<IEnumerable<CustomerResponse>> Customers(string searchText)
        {
            using var scope = _seviceProvider.CreateScope();
            var customerService = scope.ServiceProvider.GetRequiredService<ICustomerService>();
            var customers = await customerService.GetCustomers(searchText);

            if (customers == null)
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Notify", "Customers not fount");

            }
            else
            {
                await Clients.Client(Context.ConnectionId).SendAsync("Customers", customers);
            }

            return customers;
        }
    }
}
