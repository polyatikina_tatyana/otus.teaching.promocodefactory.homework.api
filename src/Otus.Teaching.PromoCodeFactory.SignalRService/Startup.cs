using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.SignalRService.Hubs;
using Otus.Teaching.PromoCodeFactory.SignalRService.Services;
using System;
using Microsoft.AspNetCore.Http.Connections;

namespace Otus.Teaching.PromoCodeFactory.SignalRService
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            services.AddScoped<ICustomerService, CustomerService>();

            services.AddSignalR(options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = TimeSpan.FromSeconds(2);
            });

            services.AddHostedService<CustomerListenerService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello World!");
                });

                endpoints.MapHub<CustomerHub>("/hubs/customer", options =>
                {
                    options.ApplicationMaxBufferSize = 1 * 1024 * 1024;

                    options.Transports = HttpTransportType.LongPolling |
                                         HttpTransportType.WebSockets |
                                         HttpTransportType.ServerSentEvents;

                    options.LongPolling.PollTimeout = TimeSpan.FromSeconds(30);
                }); // CustomerHub ����� ������������ ������� �� ���� /customer

            });

            dbInitializer.InitializeDb();
        }
    }
}
