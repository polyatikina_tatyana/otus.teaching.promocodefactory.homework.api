﻿using System;

namespace Otus.Teaching.PromoCodeFactory.SignalRService.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}