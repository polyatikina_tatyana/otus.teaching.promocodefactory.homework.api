﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        
        Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includeProperties);
        
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids, params Expression<Func<T, object>>[] includeProperties);
        
        Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        
        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        Task<long> Count(params Expression<Func<T, bool>>[] predicates);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}