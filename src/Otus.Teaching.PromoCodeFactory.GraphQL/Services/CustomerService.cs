﻿using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.GraphQL.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;


        public CustomerService(
            ILogger<CustomerService> logger,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            _logger = logger;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        public async Task<CustomerResponse> Create(CreateOrEditCustomerRequest request)
        {
            var preferences = request.PreferenceIds == null ? null : await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);
            var customer = MapToCustomer(request, preferences);

            await _customerRepository.AddAsync(customer);

            return await GetCustomer(customer.Id);
        }

        public async Task<CustomerResponse> Edit(CreateOrEditCustomerRequest request)
        {
            var customer = request.Id == null ? null : await _customerRepository.GetByIdAsync(request.Id.Value);
            if (customer == null)
            {
                _logger.LogWarning("Customer not found by id {id}", request.Id);
                return null;
            }

            var preferences = request.PreferenceIds == null ? null : await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            customer = MapToCustomer(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return await GetCustomer(customer.Id);
        }

        private Customer MapToCustomer(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Customer customer = null)
        {
            customer ??= new Customer { Id = Guid.NewGuid() };
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public async Task Delete(Guid id)
        {            
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                _logger.LogWarning("Customer not found by id {id}", id);
                return;
            }

            await _customerRepository.DeleteAsync(customer);
        }

        public async Task<CustomerResponse> GetCustomer(Guid id)
        {
            var customer = await _customerRepository
                .GetByIdAsync(id,
                x => x.Preferences.Select(y => y.Preference),
                x => x.PromoCodes.Select(y => y.PromoCode));
            if (customer == null)
            {
                _logger.LogWarning("Customer not found by id {id}", id);
                return null;
            }

            return new CustomerResponse(customer)
            {
                PromoCodes = customer.PromoCodes?.Select(x =>
                new PromoCodeShortResponse
                {
                    Id = x.PromoCode.Id,
                    Code = x.PromoCode.Code,
                    ServiceInfo = x.PromoCode.ServiceInfo,
                    PartnerName = x.PromoCode.PartnerName,
                    BeginDate = x.PromoCode.BeginDate.ToString(),
                    EndDate = x.PromoCode.ToString(),
                }).ToList(),

                Preferences = customer.Preferences?.Select(x =>
                new PreferenceResponse
                {
                    Id = x.Preference.Id,
                    Name = x.Preference.Name,
                }).ToList()
            };
        }

        public async Task<IEnumerable<CustomerResponse>> GetCustomers()
        {
            var customers = await _customerRepository.GetAllAsync(
                x => x.Preferences.Select(y => y.Preference),
                x => x.PromoCodes.Select(y => y.PromoCode));

            return customers.Select(customer => new CustomerResponse(customer)
            {
                PromoCodes = customer.PromoCodes?.Select(x =>
                new PromoCodeShortResponse
                {
                    Id = x.PromoCode.Id,
                    Code = x.PromoCode.Code,
                    ServiceInfo = x.PromoCode.ServiceInfo,
                    PartnerName = x.PromoCode.PartnerName,
                    BeginDate = x.PromoCode.BeginDate.ToString(),
                    EndDate = x.PromoCode.ToString(),
                }).ToList(),

                Preferences = customer.Preferences?.Select(x =>
                new PreferenceResponse
                {
                    Id = x.Preference.Id,
                    Name = x.Preference.Name,
                }).ToList()
            });
        }
    }
}
