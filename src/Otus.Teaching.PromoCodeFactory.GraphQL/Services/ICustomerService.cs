﻿using Otus.Teaching.PromoCodeFactory.GraphQL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Services
{
    public interface ICustomerService
    {
        Task<CustomerResponse> Create(CreateOrEditCustomerRequest request);
        Task<CustomerResponse> Edit(CreateOrEditCustomerRequest request);
        Task Delete(Guid id);
        Task<CustomerResponse> GetCustomer(Guid id);
        Task<IEnumerable<CustomerResponse>> GetCustomers();
    }
}
