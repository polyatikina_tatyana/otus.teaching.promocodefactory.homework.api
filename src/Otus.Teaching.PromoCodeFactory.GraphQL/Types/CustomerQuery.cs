﻿using HotChocolate;
using Otus.Teaching.PromoCodeFactory.GraphQL.Models;
using Otus.Teaching.PromoCodeFactory.GraphQL.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Types
{
    public class CustomerQuery
    {
        public async Task<int> Create([Service] ICustomerService customerService, CreateOrEditCustomerRequest request)
        {
            try
            {
                var response = await customerService.Create(request);
                return response != null ? 200 : 400;
            }
            catch
            {
                return 400;
            }
        }

        public async Task<int> Edit([Service] ICustomerService customerService, CreateOrEditCustomerRequest request)
        {
            try
            {
                var response = await customerService.Edit(request);
                return response != null ? 200 : 404;
            }
            catch
            {
                return 400;
            }
        }

        public async Task<int> Delete([Service] ICustomerService customerService, Guid id)
        {
            try
            {
                await customerService.Delete(id);
                return 200;
            }
            catch
            {
                return 400;
            }
        }

        public Task<CustomerResponse> GetCustomer([Service] ICustomerService customerService, Guid id)
        {
            return customerService.GetCustomer(id);
        }

        public Task<IEnumerable<CustomerResponse>> GetCustomers([Service] ICustomerService customerService)
        {
            return customerService.GetCustomers();
        }
    }
}
