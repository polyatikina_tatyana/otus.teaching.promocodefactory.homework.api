#������ �� ��������� ������ customer(id)

{
  customer(id: "a6c8c6b1-4349-45b0-ab31-244740aaf0f0")
  {
    id,
    firstName,
    lastName,
    preferences {
      id,
      name
    },
    promoCodes {
      id,
      code,
      partnerName,
      serviceInfo,
      beginDate,
      endDate
    }
  }
}


#������ �� ��������� ������ customers()

{
  customers
  {
    id,
    firstName,
    lastName,
    preferences {
      id,
      name
    },
    promoCodes {
      id,
      code,
      partnerName,
      serviceInfo,
      beginDate,
      endDate
    }
  }
}


#������ �� �������� create

{
  create(request: {
     firstName: "���������",
     lastName: "�����",
     email: "test",
     preferenceIds: ["ef7f299f-92d7-459f-896e-078ed53ef99c"]
    })
}

#������ �� �������� edit

{
  edit(request: {
     id: "a5ebda9a-3196-42be-8f09-f0e1dba7b2aa",
     firstName: "���������",
     lastName: "�������",
     email: "test",
     preferenceIds: ["c4bda62e-fc74-4256-a956-4760b3858cbd"]
    })
}

#������ �� �������� delete

{
  delete( id: "a5ebda9a-3196-42be-8f09-f0e1dba7b2aa")
}
