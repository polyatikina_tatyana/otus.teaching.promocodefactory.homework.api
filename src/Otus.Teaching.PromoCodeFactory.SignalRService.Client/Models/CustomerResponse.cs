﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.SignalRService.Client.Models
{
    public class CustomerResponse
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

    }
}