﻿using Microsoft.AspNetCore.SignalR.Client;
using Otus.Teaching.PromoCodeFactory.SignalRService.Client.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRService.Client
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/hubs/customer")
                .WithAutomaticReconnect()
                .Build();

            connection.Reconnected += (s) =>
            {
                SetListenEvents(connection);

                Console.WriteLine($"Reconnected ...");
                
                return Task.CompletedTask;
            };

            SetListenEvents(connection);

            connection.StartAsync().GetAwaiter().GetResult();

            Console.WriteLine("SignalR DEMO");
            try
            {
                connection.InvokeAsync("SayHello", "SignalR Otus").GetAwaiter().GetResult();

                GetAllCustomers(connection).GetAwaiter().GetResult();

                GetCustomer(connection).GetAwaiter().GetResult();

                AddCustomer(connection).GetAwaiter().GetResult();

                UpdateCustomer(connection).GetAwaiter().GetResult();

                DeleteCustomer(connection).GetAwaiter().GetResult();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Enter key");
                Console.ReadKey();

                connection.StopAsync().GetAwaiter().GetResult();
            }
        }

        private static void SetListenEvents(HubConnection connection)
        {
            connection.On("Notify", (string message) =>
            {
                Console.WriteLine($"[Notify]: {message}");
            });

            connection.On("Customers", (object customers) =>
            {
                Console.WriteLine($"[Customers]: {customers}");
            });

            connection.On("Customer", (object customer) =>
            {
                Console.WriteLine($"[Customer]: {customer}");
            });

            connection.On("CreateCustomer", (object customer) =>
            {
                Console.WriteLine($"[CreateCustomer]: {customer}");
            });

            connection.On("EditCustomer", (object customer) =>
            {
                Console.WriteLine($"[EditCustomer]:  {customer}");
            });

            connection.On("DeleteCustomer", (object id) =>
            {
                Console.WriteLine($"[DeleteCustomer]: id = {id}");
            });
        }

        private static async Task GetAllCustomers(HubConnection hubConnection)
        {
            Console.WriteLine("GetAllCustomers");
            try
            {
                var customers = await hubConnection.InvokeCoreAsync("Customers", typeof(IEnumerable<CustomerResponse>), new [] { string.Empty }) as IEnumerable<CustomerResponse>;
                if (customers == null) return;

                foreach (var customer in customers)
                {
                    Console.WriteLine($"Customer id = {customer.Id}, firstName = {customer.FirstName}, lastName = {customer.LastName}, email = {customer.Email}");

                    foreach (var preference in customer.Preferences)
                    {
                        Console.WriteLine($"Preferences id = {preference.Id}, name ={preference.Name}");
                    }

                    foreach (var promoCode in customer.PromoCodes)
                    {
                        Console.WriteLine($"PromoCodes id = {promoCode.Id}, code = {promoCode.Code}, partnerName = {promoCode.PartnerName}, beginDate = {promoCode.BeginDate}, endDate = {promoCode.EndDate}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private static async Task GetCustomer(HubConnection hubConnection, string id = null)
        {
            Console.WriteLine("GetCustomer");

            if (id == null)
            {
                Console.Write("Id: ");
                id = Console.ReadLine();
            }

            try
            {
                var response = await hubConnection.InvokeCoreAsync("Customer", typeof(CustomerResponse), new [] { id }) as CustomerResponse;

                Console.WriteLine($"GetCustomer id = {response?.Id}, firstName = {response?.FirstName}, lastName = {response?.LastName}, email = {response?.Email}");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }
        private static async Task AddCustomer(HubConnection hubConnection)
        {
            Console.WriteLine("Add new customer");

            Console.Write("FirstName: ");
            var firstName = Console.ReadLine();

            Console.Write("LastName: ");
            var lastName = Console.ReadLine();

            Console.Write("Email: ");
            var email = Console.ReadLine();

            var createRequest = new CreateOrEditCustomerRequest
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PreferenceIds = new List<Guid> { Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c") }
            };

            createRequest.PreferenceIds.Add(Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"));

            try
            {
                var createResponse = await hubConnection.InvokeCoreAsync("Create", typeof(CustomerResponse), new[] { createRequest }) as CustomerResponse;
                Console.WriteLine($"CreateCustomer id = {createResponse?.Id}, firstName = {createResponse?.FirstName}, lastName = {createResponse?.LastName}, email = {createResponse?.Email}");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private static async Task UpdateCustomer(HubConnection hubConnection)
        {
            Console.WriteLine("Update customer");

            Console.Write("Id: ");
            var id = Console.ReadLine();

            Console.Write("FirstName: ");
            var firstName = Console.ReadLine();

            Console.Write("LastName: ");
            var lastName = Console.ReadLine();

            Console.Write("Email: ");
            var email = Console.ReadLine();

            var createRequest = new CreateOrEditCustomerRequest
            {
                Id = Guid.Parse(id),
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PreferenceIds = new List<Guid> { Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") }
            };

            try
            {
                var editResponse = await hubConnection.InvokeCoreAsync("Edit", typeof(CustomerResponse), new[] { createRequest }) as CustomerResponse;

                Console.WriteLine($"EditCustomer id = {editResponse?.Id}, firstName = {editResponse?.FirstName}, lastName = {editResponse?.LastName}, email = {editResponse?.Email}");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private static async Task DeleteCustomer(HubConnection hubConnection)
        {
            Console.WriteLine("Delete customer");

            Console.Write("Id: ");
            var id = Console.ReadLine();

            try
            {
                await hubConnection.InvokeAsync("Delete", id);
                Console.WriteLine($"DeleteCustomer Ok");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                return;
            }

            await GetCustomer(hubConnection, id);
        }
    }
}
