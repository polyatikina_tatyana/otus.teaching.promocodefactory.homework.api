﻿using System;
using System.Linq.Expressions;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Extensions
{
    internal static class QueryableExtension
    {
        public static IQueryable<TEntity> IncludeBy<TEntity>(this IQueryable<TEntity> query,
           params Expression<Func<TEntity, object>>[] includeProperties)
           where TEntity : class
        {
            if (includeProperties == null) return query;

            return includeProperties
                .Aggregate(query, (current, includeProperty)
                 => current.Include(includeProperty.AsIncludePath()));
        }
    }
}
