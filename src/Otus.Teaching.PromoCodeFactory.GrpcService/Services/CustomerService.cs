﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Domain = Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using System.Linq;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.GrpcService.Services
{
    public class CustomerService : Customer.CustomerBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Domain.Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;


        public CustomerService(
            ILogger<CustomerService> logger,
            IRepository<Preference> preferenceRepository,
            IRepository<Domain.Customer> customerRepository)
        {
            _logger = logger;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        public override async Task<CustomerResponse> CreateCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {
            if (request.PreferenceIds?.Any(x => !Guid.TryParse(x, out _)) == true)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect preferenceIds"));
            }

            var preferenceIds = request.PreferenceIds?.Select(Guid.Parse).ToList();
            var preferences = preferenceIds == null ? null : await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);
            var customer = MapToCustomer(request, preferences);

            await _customerRepository.AddAsync(customer);

            return await GetCustomer(new CustomerRequest { Id = customer.Id.ToString() }, context);
        }

        public override async Task<CustomerResponse> EditCustomer(CreateOrEditCustomerRequest request, ServerCallContext context)
        {            
            if (!Guid.TryParse(request.Id, out var customerId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect customer id"));
            }

            if (request.PreferenceIds?.Any(x => !Guid.TryParse(x, out _)) == true)
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect preferenceIds"));
            }

            var customer = await _customerRepository.GetByIdAsync(customerId);
            if (customer == null)
            {
                _logger.LogWarning("Customer not found by id {id}", request.Id);
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));
            }

            var preferenceIds = request.PreferenceIds?.Select(Guid.Parse).ToList();
            var preferences = preferenceIds == null ? null : await _preferenceRepository.GetRangeByIdsAsync(preferenceIds);
            
            customer = MapToCustomer(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return await GetCustomer(new CustomerRequest { Id = request.Id }, context);
        }

        private Domain.Customer MapToCustomer(CreateOrEditCustomerRequest model, IEnumerable<Preference> preferences, Domain.Customer customer = null)
        {
            customer ??= new Domain.Customer { Id = Guid.NewGuid() };
            customer.FirstName = model.FirstName;
            customer.LastName = model.LastName;
            customer.Email = model.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            return customer;
        }

        public override async Task<Empty> DeleteCustomer(CustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var customerId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect customer id"));
            }

            var customer = await _customerRepository.GetByIdAsync(customerId);
            if (customer == null)
            {
                _logger.LogWarning("Customer not found by id {id}", request.Id);
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));
            }

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }

        public override async Task<CustomerResponse> GetCustomer(CustomerRequest request, ServerCallContext context)
        {
            if (!Guid.TryParse(request.Id, out var customerId))
            {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "Incorrect customer id"));
            }

            var customer = await _customerRepository
                .GetByIdAsync(customerId,
                x => x.Preferences.Select(y => y.Preference),
                x => x.PromoCodes.Select(y => y.PromoCode));
            if (customer == null)
            {
                _logger.LogWarning("Customer not found by id {id}", request.Id);
                throw new RpcException(new Status(StatusCode.NotFound, "Customer not found"));
            }

            var customerResponse = new CustomerResponse()
            {
                Id = customer.Id.ToString(),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };

            customer.PromoCodes?.ToList().ForEach(x => customerResponse.PromoCodes.Add(
                new PromoCodeShortResponse
                {
                    Id = x.PromoCode.Id.ToString(),
                    Code = x.PromoCode.Code,
                    ServiceInfo = x.PromoCode.ServiceInfo,
                    PartnerName = x.PromoCode.PartnerName,
                    BeginDate = x.PromoCode.BeginDate.ToString(),
                    EndDate = x.PromoCode.ToString(),
                }));

            customer.Preferences?.ToList().ForEach(x => customerResponse.Preferences.Add(
                new PreferenceResponse
                {
                    Id = x.Preference.Id.ToString(),
                    Name = x.Preference.Name,
                }));

            return customerResponse;
        }

        public override async Task GetCustomers(Empty request, IServerStreamWriter<CustomerResponse> responseStream, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync(
                x => x.Preferences.Select(y => y.Preference),
                x => x.PromoCodes.Select(y => y.PromoCode));

            foreach (var customer in customers)
            { 
                var customerResponse = new CustomerResponse()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                };

                customer.PromoCodes?.ToList().ForEach(x => customerResponse.PromoCodes.Add(
                    new PromoCodeShortResponse
                {
                    Id = x.PromoCode.Id.ToString(),
                    Code = x.PromoCode.Code,
                    ServiceInfo = x.PromoCode.ServiceInfo,
                    PartnerName = x.PromoCode.PartnerName,
                    BeginDate = x.PromoCode.BeginDate.ToString(),
                    EndDate = x.PromoCode.ToString(),
                }));

                customer.Preferences?.ToList().ForEach(x => customerResponse.Preferences.Add(
                    new PreferenceResponse
                {
                    Id = x.Preference.Id.ToString(),
                    Name = x.Preference.Name,
                }));

                await responseStream.WriteAsync(customerResponse);
            }
        }
    }
}
