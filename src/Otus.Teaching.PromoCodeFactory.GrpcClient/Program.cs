﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GrpcService;
using System;
using System.Threading.Tasks;
using static Otus.Teaching.PromoCodeFactory.GrpcService.Customer;

namespace Otus.Teaching.PromoCodeFactory.GrpcClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001/");
            var customerClient = new Customer.CustomerClient(channel);

            Console.WriteLine("GRPC DEMO");

            GetAllCustomers(customerClient).GetAwaiter().GetResult();
 
            GetCustomer(customerClient).GetAwaiter().GetResult();

            AddCustomer(customerClient).GetAwaiter().GetResult();

            UpdateCustomer(customerClient).GetAwaiter().GetResult();

            DeleteCustomer(customerClient).GetAwaiter().GetResult();

            Console.WriteLine("Enter key");
            Console.ReadKey();
        }

        private static async Task GetAllCustomers(Customer.CustomerClient customerClient)
        {
            Console.WriteLine("GetAllCustomers");
            try
            {
                var call = customerClient.GetCustomers(new Empty(), deadline: DateTime.UtcNow.AddMinutes(1));
                
                await foreach (var response in call.ResponseStream.ReadAllAsync())
                {
                    Console.WriteLine($"Customer id = {response.Id}, firstName = {response.FirstName}, lastName = {response.LastName}, email = {response.Email}");
                    
                    foreach (var preference in response.Preferences)
                    {
                        Console.WriteLine($"Preferences id = {preference.Id}, name ={preference.Name}");
                    }

                    foreach (var promoCode in response.PromoCodes)
                    {
                        Console.WriteLine($"PromoCodes id = {promoCode.Id}, code = {promoCode.Code}, partnerName = {promoCode.PartnerName}, beginDate = {promoCode.BeginDate}, endDate = {promoCode.EndDate}");
                    }
                }
            }
            catch (RpcException ex)
            {
                switch (ex.StatusCode)
                {
                    case StatusCode.DeadlineExceeded:
                        Console.WriteLine("GetAllCustomers timeout.");
                        return;
                }

                Console.Error.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private static async Task GetCustomer(Customer.CustomerClient customerClient, string id = null)
        {
            Console.WriteLine("GetCustomer");

            if (id == null)
            {
                Console.Write("Id: ");
                id = Console.ReadLine();
            }

            try
            {
               var response = await customerClient.GetCustomerAsync(new CustomerRequest { Id = id }, deadline: DateTime.UtcNow.AddSeconds(15));

               Console.WriteLine($"GetCustomer id = {response.Id}, firstName = {response.FirstName}, lastName = {response.LastName}, email = {response.Email}");
            }
            catch (RpcException ex)
            {
                switch (ex.StatusCode)
                {
                    case StatusCode.NotFound:
                        Console.WriteLine("Customer not found.");
                        return;

                    case StatusCode.DeadlineExceeded:
                        Console.WriteLine("GetCustomer timeout.");
                        return;
                }

                Console.Error.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }
        private static async Task AddCustomer(Customer.CustomerClient customerClient)
        {
            Console.WriteLine("Add new customer");

            Console.Write("FirstName: ");
            var firstName = Console.ReadLine();

            Console.Write("LastName: ");
            var lastName = Console.ReadLine();

            Console.Write("Email: ");
            var email = Console.ReadLine();

            var createRequest = new CreateOrEditCustomerRequest
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };

            createRequest.PreferenceIds.Add("ef7f299f-92d7-459f-896e-078ed53ef99c");

            try
            {
                var createResponse = await customerClient.CreateCustomerAsync(createRequest, deadline: DateTime.UtcNow.AddSeconds(30));
                Console.WriteLine($"CreateCustomer id = {createResponse.Id}, firstName = {createResponse.FirstName}, lastName = {createResponse.LastName}, email = {createResponse.Email}");
            }
            catch(RpcException ex)
            {
                switch (ex.StatusCode)
                {
                    case StatusCode.DeadlineExceeded:
                       Console.WriteLine("AddCustomer timeout.");
                        return;
                }

                Console.Error.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private static async Task UpdateCustomer(Customer.CustomerClient customerClient)
        {
            Console.WriteLine("Update customer");

            Console.Write("Id: ");
            var id = Console.ReadLine();

            Console.Write("FirstName: ");
            var firstName = Console.ReadLine();

            Console.Write("LastName: ");
            var lastName = Console.ReadLine();

            Console.Write("Email: ");
            var email = Console.ReadLine();

            var createRequest = new CreateOrEditCustomerRequest
            {
                Id = id,
                FirstName = firstName,
                LastName = lastName,
                Email = email
            };

            createRequest.PreferenceIds.Add("c4bda62e-fc74-4256-a956-4760b3858cbd");

            try
            {
               var editResponse = await customerClient.EditCustomerAsync(createRequest, deadline: DateTime.UtcNow.AddMinutes(1));

               Console.WriteLine($"EditCustomer id = {editResponse.Id}, firstName = {editResponse.FirstName}, lastName = {editResponse.LastName}, email = {editResponse.Email}");
            }
            catch (RpcException ex)
            {
                switch (ex.StatusCode)
                {
                    case StatusCode.NotFound:
                        Console.WriteLine("Customer not found.");
                        return;

                    case StatusCode.DeadlineExceeded:
                        Console.WriteLine("UpdateCustomer timeout.");
                        return;
                }

                Console.Error.WriteLine(ex.ToString());
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
            }
        }

        private static async Task DeleteCustomer(Customer.CustomerClient customerClient)
        {
            Console.WriteLine("Delete customer");

            Console.Write("Id: ");
            var id = Console.ReadLine();

            try
            {
                await customerClient.DeleteCustomerAsync(new CustomerRequest { Id = id }, deadline: DateTime.UtcNow.AddSeconds(15));
                Console.WriteLine($"DeleteCustomer Ok");
            }
            catch (RpcException ex)
            {
                switch (ex.StatusCode)
                {
                    case StatusCode.NotFound:
                        Console.WriteLine("Customer not found.");
                        return;

                    case StatusCode.DeadlineExceeded:
                        Console.WriteLine("DeleteCustomer timeout.");
                        return;
                }

                Console.Error.WriteLine(ex.ToString());
                return;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                return;
            }

            await GetCustomer(customerClient, id);
        }
    }
}
